﻿USE compranp;

/* 
  PROCEDIMIENTO 1
  LISTAR EL NOMBRE DE LOS PRODUCTOS CUYO PRECIO ES 
  MAYOR QUE LA MEDIA 
*/

-- REALIZARLO CON UNA CONSULTA DE SELECCION

-- C1
-- PRECIO MEDIO
SELECT AVG(p.precio) AS precioMedio FROM productos p;

-- TERMINAMOS LA CONSULTA
SELECT 
    * 
  FROM 
    productos p
  WHERE 
    p.precio>(SELECT AVG(p.precio) AS precioMedio FROM productos p);
    
-- UTILIZAR UNA VARIABLE QUE ALMACENE EL VALOR DE C1

set @c1=(SELECT AVG(p.precio) AS precioMedio FROM productos p);

-- AHORA REALIZO LA MISMA CONSULTA PERO CON LA VARIABLE

-- CREO LA VARIABLE
set @c1=(SELECT AVG(p.precio) AS precioMedio FROM productos p);

-- REALIZO LA CONSULTA UTILIZANDO LA VARIABLE
SELECT 
    * 
  FROM 
    productos p
  WHERE 
    p.precio>@c1;

-- VOY A REALIZAR LA MISMA CONSULTA PERO EN UN PROCEDIMIENTO

DROP PROCEDURE IF EXISTS procedimiento1;

DELIMITER //

CREATE PROCEDURE procedimiento1()
BEGIN
  -- CREO LA VARIABLE
  set @c1=(SELECT AVG(p.precio) AS precioMedio FROM productos p);
  
  -- REALIZO LA CONSULTA UTILIZANDO LA VARIABLE
  SELECT 
      * 
    FROM 
      productos p
    WHERE 
      p.precio>@c1;
END //

DELIMITER ;

CALL procedimiento1();

-- REALIZO EL MISMO PROCEDIMIENTO PERO CON 
-- VARIABLES DECLARADAS

DROP PROCEDURE IF EXISTS procedimiento1;

DELIMITER //

CREATE PROCEDURE procedimiento1()
BEGIN
  -- DECLARO LA VARIABLE
  DECLARE c1 float DEFAULT 0;

  -- ASIGNAR UN VALOR A LA VARIABLE
  set c1=(SELECT AVG(p.precio) AS precioMedio FROM productos p);
  
  -- REALIZO LA CONSULTA UTILIZANDO LA VARIABLE
  SELECT 
      * 
    FROM 
      productos p
    WHERE 
      p.precio>c1;
END //

DELIMITER ;

CALL procedimiento1();


-- PROCEDIMIENTO 2
-- INDICAR EL CODIGO DE LOS PRODUCTOS CUYOS INGRESOS ESTEN POR 
-- ENCIMA DE LOS INGRESOS MEDIOS DE TODOS LOS PRODUCTOS

-- calculo el ingreso medio
SELECT 
    AVG(c.total) AS media
  FROM compranp c;

-- CODIGO DE LOS PRODUCTOS Y SUS INGRESOS MEDIOS
SELECT 
    c.codPro,
    AVG(c2.total) AS mediaProducto 
  FROM 
    compran c JOIN compranp c2 ON c.idCompran = c2.idCompran
  GROUP BY c.codPro;

-- MONTAMOS LA CONSULTA FINAL

SELECT 
    c.codPro,
    AVG(c2.total) AS mediaProducto 
  FROM 
    compran c JOIN compranp c2 ON c.idCompran = c2.idCompran
  GROUP BY c.codPro
  HAVING 
    mediaProducto>(
      SELECT 
          AVG(c.total) AS media
        FROM compranp c
    );

-- CREO UN PROCEDIMIENTO CON ESTE CODIGO

DROP PROCEDURE IF EXISTS procedimiento2;

DELIMITER //

CREATE PROCEDURE procedimiento2()
BEGIN

  SELECT 
      c.codPro,
      AVG(c2.total) AS mediaProducto 
    FROM 
      compran c JOIN compranp c2 ON c.idCompran = c2.idCompran
    GROUP BY c.codPro
    HAVING 
      mediaProducto>(
        SELECT 
            AVG(c.total) AS media
          FROM compranp c
      );
END //

DELIMITER ;

CALL procedimiento2();

-- PROCEDIMIENTO CON VARIABLE

DROP PROCEDURE IF EXISTS procedimiento2;

DELIMITER //

CREATE PROCEDURE procedimiento2()
BEGIN
  -- DECLARO LA VARIABLE
  DECLARE precioMedio float DEFAULT 0;
  -- ASIGNO EL VALOR A LA VARIABLE
  SET precioMedio=(
      SELECT 
          AVG(c.total) AS media
        FROM compranp c
  );

  SELECT 
      c.codPro,
      AVG(c2.total) AS mediaProducto 
    FROM 
      compran c JOIN compranp c2 ON c.idCompran = c2.idCompran
    GROUP BY c.codPro
    HAVING 
      mediaProducto>precioMedio;
END //

DELIMITER ;